﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.WebService.Otli.Statistic.Models
{
    public class StoreInOneCityBindingModel
    {
		public int Id { get; set; }
		public string Name { get; set; }
		public int Number { get; set; }
		public string Description { get; set; }
		public int OwnerId { get; set; }

		public string CityName { get; set; }
		public string CityDescription { get; set; }

		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string SurName { get; set; }
	}
}
