﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Security.Claims;
using DreamPlace.WebService.Otli.Statistic.Models.ApplicationModels;
using Microsoft.AspNetCore.Identity;

namespace DreamPlace.WebService.Otli.Statistic.Models
{
	// Add profile data for application users by adding properties to the ApplicationUser class
	public class ApplicationUser : IdentityUser
	{
		//public string SocialEmail { get; set; }
		//public string SocialId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string SurName { get; set; }
		public ICollection<Store> Stores { get; set; }
		public string Image { get; set; }
		public ApplicationUser()
		{
			Stores = new List<Store>();
		}

		//public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
		//{
		//	// Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
		//	ClaimsIdentity userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
		//	// Add custom user claims here
		//	return userIdentity;
		//}

		//public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager,
		//	string authenticationType)
		//{
		//	//// Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
		//	//var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
		//	//// Add custom user claims here
		//	//return userIdentity;
		//}
	}
}
