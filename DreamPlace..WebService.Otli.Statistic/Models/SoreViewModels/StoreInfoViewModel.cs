﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.WebService.Otli.Statistic.Models.SoreViewModels
{
    public class StoreInfoViewModel
    {
		[Display(Name="Название")]
		public string Title { get; set; }
		[Display(Name = "Адрес")]
		public string Address { get; set; }
		[Display(Name = "Выручка за последнюю неделю")]
		public decimal CashOnLastWeek { get; set; }
		[Display(Name = "Выручка за всё время")]
		public decimal Cash { get; set; }
		[Display(Name = "Руководитель")]
		public string Owner { get; set; }
	}
}
