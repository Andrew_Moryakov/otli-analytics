﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DreamPlace.WebService.Otli.Statistic.Data;
using Microsoft.EntityFrameworkCore;

namespace DreamPlace.WebService.Otli.Statistic.Models.UoW
{
	public class UnitOfWork : IDisposable
	{
		private readonly ApplicationDbContext _context;
		private bool _disposed;
		private Dictionary<string, object> _repositories;

		public UnitOfWork(ApplicationDbContext context)
		{
			this._context = context;
		}

		public UnitOfWork(DbContextOptions<ApplicationDbContext> option)
		{
			_context = new ApplicationDbContext(option);
		}

		public void Save() => _context.SaveChanges();

		public UniversalRepository<T> Repository<T>() where T : class//BaseEntity
		{
			if (_repositories == null)
			{
				_repositories = new Dictionary<string, object>();
			}

			string type = typeof(T).Name;

			if (!_repositories.ContainsKey(type))
			{
				Type repositoryType = typeof(UniversalRepository<>);
				var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), _context);
				_repositories.Add(type, repositoryInstance);
			}
			return (UniversalRepository<T>)_repositories[type];
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				if (disposing)
				{
					_context.Dispose();
				}
			}
			_disposed = true;
		}
	}
}
