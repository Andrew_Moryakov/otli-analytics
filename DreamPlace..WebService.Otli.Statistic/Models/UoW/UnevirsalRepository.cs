﻿using System;
using System.Collections.Generic;
using System.Linq;
using DreamPlace.WebService.Otli.Statistic.Data;
using Microsoft.EntityFrameworkCore;

namespace DreamPlace.WebService.Otli.Statistic.Models.UoW
{
	public sealed class UniversalRepository<T> where T : class
	{
		private readonly ApplicationDbContext _context;
		private DbSet<T> _entities;

		public UniversalRepository(ApplicationDbContext context)
		{
			this._context = context;
		}

		public IEnumerable<T> Get()
		{
			return Entities;
		}

		public T FindById(object id)
		{
			return this.Entities.Find(id);
		}

		public object Insert(T entity)
		{
			if (entity == null)
				throw new ArgumentNullException(nameof(entity));

			this.Entities.Add(entity);

			return entity;
		}

		public object Update(T entity)
		{
			if (entity == null)
			{
				throw new ArgumentNullException(nameof(entity));
			}

			return entity;
		}

		public object Delete(T entity)
		{
			if (entity == null)
			{
				throw new ArgumentNullException(nameof(entity));
			}

			this.Entities.Remove(entity);

			return entity;
		}

		public IQueryable<T> Table
		{
			get {
				return this.Entities;
			}
		}

		private DbSet<T> Entities
		{
			get
			{
				if (_entities == null)
				{
					_entities = _context.Set<T>();
				}

				return _entities;
			}
		}
	}
}
