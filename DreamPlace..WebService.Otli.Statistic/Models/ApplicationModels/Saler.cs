﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DreamPlace.WebService.Otli.Statistic.Models.ApplicationModels
{
	public class Saler
	{
		[Key]
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string SurName { get; set; }

		public int StoreId { get; set; }
		[ForeignKey("PartnerId")]
		public Store Store { get; set; }

		public string Image { get; set; }
	}
}