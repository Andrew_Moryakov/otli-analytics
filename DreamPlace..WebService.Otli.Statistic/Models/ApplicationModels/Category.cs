﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.WebService.Otli.Statistic.Models.ApplicationModels
{
    public class Category
    {
	    public Category()
	    {
		    Products = new List<Product>();
	    }

		[Key]
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTime DataCreate { get; set; }
		public string Image { get; set; }
		public bool IsDeleted { get; set; } = false;
		public bool IsActive { get; set; } = true;
		public IList<Product> Products { get; set; }
	}
}
