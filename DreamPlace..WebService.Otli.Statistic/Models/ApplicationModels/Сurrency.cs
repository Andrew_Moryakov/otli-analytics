﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.WebService.Otli.Statistic.Models.ApplicationModels
{
    public class Сurrency
    {
		public int Id { get; set; }
		public string Name { get; set; }
		public string ShortName { get; set; }
		public CultureInfo Contry { get; set; }
    }
}
