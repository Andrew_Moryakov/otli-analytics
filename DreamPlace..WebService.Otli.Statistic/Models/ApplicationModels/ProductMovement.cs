﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.WebService.Otli.Statistic.Models.ApplicationModels
{
    public class ProductMovement
    {
	    public ProductMovement()
	    {
	    }

		[Key]
		public int Id { get; set; }
		public DateTime OneDate { get; set; }
		public DateTime TwoDate { get; set; }
		public string Description { get; set; }
		public int Amount { get; set; }
		public decimal PriceByPeer { get; set; }

		public int ProductId { get; set; }
		[ForeignKey("ProductId")]
		public Product SaledProduct { get; set; }

		public int StoreId { get; set; }
		[ForeignKey("StoreId")]
		public Store Store { get; set; }

		public string ApplicationUserId { get; set; }
		[ForeignKey("ApplicationUserId")]
		public ApplicationUser Saler { get; set; }

		public int ValuteId { get; set; }
		[ForeignKey("ValuteId")]
		public Сurrency Сurrency { get; set; }
	}
}