﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.WebService.Otli.Statistic.Models.ApplicationModels
{
	public class City
	{
		public City()
		{
			Stores = new List<Store>();
		}

		[Key]
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public ICollection<Store> Stores { get; set; }

	}
}