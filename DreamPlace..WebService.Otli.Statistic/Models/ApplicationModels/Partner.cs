﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DreamPlace.WebService.Otli.Statistic.Models.ApplicationModels
{
	public class Partner
	{
		public Partner()
		{
			Stores = new List<Store>();
		}

		[Key]
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string SurName { get; set; }
		public ICollection<Store> Stores { get; set; }
		public string Image { get; set; }
	}
}