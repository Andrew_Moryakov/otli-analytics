﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.WebService.Otli.Statistic.Models.ApplicationModels
{
	public class Product
	{
		public Product()
		{
			Categories = new List<Category>();
		}

		[Key]
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public decimal Price { get; set; }
		public DateTime DataCreate { get; set; }
		public string Image { get; set; }
		public bool IsDeleted { get; set; } = false;
		public bool IsActive { get; set; } = true;
		public ICollection<Category> Categories { get; set; }

		public int СurrencyId { get; set; }
		[ForeignKey("СurrencyId")]
		public Сurrency Сurrency { get; set; }
	}
}
