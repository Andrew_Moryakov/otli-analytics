﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.WebService.Otli.Statistic.Models.ApplicationModels
{
    public class Store
    {
	    public Store()
	    {
			Sellers = new List<ApplicationUser>();
	    }

		[Key]
	    public int Id { get; set; }
		public string Name { get; set; }
		public int Number { get; set; }
		public string Description { get; set; }
		public ICollection<ApplicationUser> Sellers { get; set; }

		public int OwnerId { get; set; }
		[ForeignKey("OwnerId")]
		public ApplicationUser Owner { get; set; }

    }
}
