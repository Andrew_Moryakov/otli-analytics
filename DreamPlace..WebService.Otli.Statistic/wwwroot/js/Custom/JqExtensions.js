﻿(function( $ ) {

	$.fn.replaceContentWith = function(newContent) {
		this.empty();
		this.append(newContent);
	};


})( jQuery );