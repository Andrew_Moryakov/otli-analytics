﻿app.controller("StoreListCtrl",
	function($scope, $http, $sessionStorage, $window) {
		$scope.$storage = $sessionStorage;

		$scope.home = 'Store list controller';
	});

function replace(t, o, n) {
	return t.split(o).join(n);
}
function sum(one, two) {

     one = Number(parseFloat(one).toFixed(2)) + Number(parseFloat(two).toFixed(2));
	 return parseFloat(one).toFixed(2);
 };