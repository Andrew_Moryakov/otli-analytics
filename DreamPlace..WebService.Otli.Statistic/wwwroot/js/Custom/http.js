﻿var result;
function runPost(api, dataToServ, token, type) {
	var obj = dataToServ;
	$.ajax({
		type: type,
		async: false,
		url: api,
		dataType: "json",
		data: obj,
		success: function(data) {
			result = data;
		},
		beforeSend: function (xhr) {
			if (token != null && token != undefined) {
				xhr.setRequestHeader("Authorization", "Bearer " + token);
			}
		},
		error: function (err) {
			result = err;
		}
	});
	return result;
}

function getToken(email, password) {

	var loginData = {
                grant_type: 'password',
				username: email,
				password: password
            };

	$.ajax({
		type: "POST",
		async: false,
		url: "/Token",
		dataType: "json",
		data: loginData,
		success: function(data) {
			result = data;
			
		},
		error: function (err) {
			result = err;
		}
	});
	return result;
}


function post(path, params, method) {
	method = method || "post";
	var form = document.createElement("form");
	form.setAttribute("method", method);
	form.setAttribute("action", path);

	for (var key in params) {
		if (params.hasOwnProperty(key)) {
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", key);
			hiddenField.setAttribute("value", params[key]);

			form.appendChild(hiddenField);
		}
	}

	document.body.appendChild(form);
	form.submit();
}