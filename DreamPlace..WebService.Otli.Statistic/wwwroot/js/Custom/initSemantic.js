﻿$(document).ready(function() {


	$('.menu .item').tab();
	setTimeout(function() {
			$('a[ng-click]').first().trigger('click');
		},
		100);

	$('.shape').shape();
	$('.ui.rating').rating();
	$('.menu .item').tab();
	$(".ui.fluid.multiple.search.selection.dropdown").dropdown({ allowAdditions: true });

	$('.button.ascending').popup({
		delay: {
			show: 300,
			hide: 0
		}
	});
	$('.button.descending').popup({
		delay: {
			show: 300,
			hide: 0
		}
	});


	// dropDownTemplate = $('<div class="ui fluid multiple search selection dropdown"> <input name="tags" type="hidden"> <i class="dropdown icon"></i> <div class="default text">Skills</div> <div class="menu"></div></div>').dropdown({ allowAdditions: true });
	$('#categoryProduct')
		.dropdown();


	$('.ui.labeled.icon.button').click(function() { 
				$('.ui.sidebar')
					.sidebar('toggle');

			});


	$('.ui.red.table')
		.on('click',
			'tr',
			function () {
				$('tr').removeClass('active');
				$(this)
					.addClass('active')
					.siblings('.item')
					.removeClass('active');

			});

			$('body')
		.on('click',
			'.dimmer',
			function () {
				$('.dimmer').removeClass('active');
			});

});
