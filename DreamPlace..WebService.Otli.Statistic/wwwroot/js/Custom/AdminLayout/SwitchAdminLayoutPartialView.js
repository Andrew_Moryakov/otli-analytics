﻿    function CustomerEdit() {

	    this.init = function() {
		    $("#SwitchStore_button").click(function() {
			    GetPartialView("/Stores/Index", $("#SwitchStore_button"));
		    });

		    $("#SwitchOwners_button").click(function() {
			    GetPartialView("/Owners/Index", $("#SwitchOwners_button"));
		    });

		    $("#SwitchToCreateStorePage_Button").click(function() {
			    GetPartialView("/Stores/Create", $("#SwitchOwners_button"));
		    });
	    }
    }

    function GetPartialView(url, button) {
		if (!button.parent().hasClass('open')) {
			$.ajax({
				type: "POST",
				url: url,
				success: function(data) {
					AdminLayout.replaceContentWith(data);

				}
			});
		}

	}

    $(document).ready(function () {
 new CustomerEdit().init();
	});

	        //$(document).on("click", ".remove-line", function () {
	        //    $(this).closest(".OwnershipWrapper").remove();
	        //});