﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using DreamPlace.WebService.Otli.Statistic.Models;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace DreamPlace.WebService.Otli.Statistic.Data
{
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
			: base(options)
		{
		}

		//public override DbSet<TEntity> Set<TEntity>()
		//{
		//	return  this.Set<TEntity>();
		//}
	}
}