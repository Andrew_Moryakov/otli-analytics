﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DreamPlace.WebService.Otli.Statistic.Data;
using DreamPlace.WebService.Otli.Statistic.Models.ApplicationModels;
using DreamPlace.WebService.Otli.Statistic.Models.UoW;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace DreamPlace.WebService.Otli.Statistic.Controllers.WebApiControllers
{
    [Produces("application/json")]
    [Route("api/Store")]
    public class StoreController : Controller
    {
        // GET: api/Store
   //     [HttpGet]
   //     public IEnumerable<string> Get()
   //     {
			////UnitOfWork uow = new UnitOfWork(new DbContextOptions<ApplicationDbContext>());

			////UniversalRepository<Сurrency> storeRep = uow.Repository<Сurrency>();
	  ////      return storeRep.Get().Select(el=>el?.Name);
   //     }

        // GET: api/Store/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }
        
        // POST: api/Store
        [HttpPost]
        public void Post(Store model)
        {
        }
        
        // PUT: api/Store/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
