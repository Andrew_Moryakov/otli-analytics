using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DreamPlace.WebService.Otli.Statistic.Controllers
{
    public class OwnersController : Controller
    {
        // GET: Owners
        public ActionResult Index()
        {
            return PartialView("~/Views/Owners/Index.cshtml");
        }

        //// GET: Owners/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        // GET: Owners/Create
        public ActionResult Create()
        {
            return View();
        }

        //// GET: Owners/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// GET: Owners/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}
    }
}