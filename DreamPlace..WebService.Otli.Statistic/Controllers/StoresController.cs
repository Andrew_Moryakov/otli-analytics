﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DreamPlace.WebService.Otli.Statistic.Controllers
{
	public class StoresController : Controller
	{
		// GET: Owners
		public ActionResult Index()
		{
			return PartialView("~/Views/Stores/Index.cshtml");
		}

		[HttpPost]
		public ActionResult Create()
		{
			return PartialView("~/Views/Stores/Create.cshtml");
		}

		//// GET: Owners/Edit/5
		//public ActionResult Edit(int id)
		//{
		//    return View();
		//}

		//// GET: Owners/Delete/5
		//public ActionResult Delete(int id)
		//{
		//    return View();
		//}
	}
}